# stationeers-docker

Docker setup for a dedicated server of the game [Stationeers](https://store.steampowered.com/app/544550/Stationeers/).


## setup
```sh
git clone "https://gitlab.com/rickra/stationeers-docker.git"
cd stationeers-docker
```
the saves and settings are bind mounted to the respective directories, the settings file is currently fixed to `settings/setting.xml` and will be overridden by the server on launch (including the previous values).
you can save your edits as a `git commit` and `git restore settings/setting.xml` to restore them for making changes.

bind mounts mean you will have to worry about your uid matching the one in the container (most likely `1000`, it may just work for you if you are the only user on your system).
if you are using docker and dont want to deal with `chown`ing these folders you can also change the mounts `compose.yaml` to be [volumes](https://docs.docker.com/compose/compose-file/#volumes) (make the first element not a path, this will be the label).
you can then edit the files after initially launching the server to fit your needs.

settings can also be passed via the `-settings` argument followed by `key` `value` pairs (see [wiki](https://stationeers-wiki.com/Dedicated_Server_Guide)).
the savefilename and worldtype are also passed via arguments(`-load <savename> <type>`) and will need to be set in the `compose.yaml` before building.

when all settings are to your liking you can start the server with:
```sh
docker-compose up
```


## is my server online?
usually when the server is working it prints on the screen something like this:
```json
>                                                                                                                                                                                                                                                                              s
Log: connection {1} has been disconnected by timeout; address {<redacted>} time {<redacted>}, last rec time {<redacted>} rtt {0} timeout {2000}
  "mapName": "Moon",
  "port": 27016,
  "password": false,
  "maxPlayers": 10,
  "ipAddress": "<redacted>"
}
```
if this does not appear on your screen your mount permissions are probably fucked up.
note that the ip does not need to be correct.

you can also test the connection with
```sh
xxd -r -p <<< "0000013e3b14e70001000001000300bf2bcbc0" | nc -u <host> <gameport(27016 by default)> | xxd
```
the packet is captured from a real connection attempt, i have no idea whats in it.
if some random garabge appears on your screen the server should be working.


## issues
### `Rethrow as UnauthorizedAccessException: Access to the path '/home/steam/stationeers/saves/default' is denied`
this issue is due to the UID from inside the container being different from your UID outside the container.
