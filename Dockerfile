FROM cm2network/steamcmd:root

RUN apt-get update && apt-get upgrade -y && \
	apt-get install -y tmux && \
	rm -rf /var/lib/apt/lists/*

USER steam
ENV INSTALLDIR="/home/steam/stationeers/"
RUN ./steamcmd.sh +force_install_dir "$INSTALLDIR" +login anonymous +app_update 600760 validate +quit &&\
	mkdir "$INSTALLDIR/settings" && ln -s "$INSTALLDIR/settings/setting.xml" "$INSTALLDIR/setting.xml"
WORKDIR "$INSTALLDIR"

ENTRYPOINT ["tmux", "new", "./rocketstation_DedicatedServer.x86_64"]
